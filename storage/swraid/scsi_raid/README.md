# storage/swraid/scsi_raid test suite

Storage: create scsi raid with loop device
Test Maintainer: [Changhui Zhong](mailto:czhong@redhat.com)

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
